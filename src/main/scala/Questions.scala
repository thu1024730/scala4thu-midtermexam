import scala.util.Random

/**
  * Created by mark on 08/04/2017.
  */
object Questions {
  /** 5%
    * 請用Tail Recursive寫出階乘函數fac(n)=n*(n-1)*...*2*1
    * ex:
    * tailFac(5)=120
    * */
  def tailFac(n:Int, acc:Int)=(Int, Int)={
    if (n == 1) 1
    else n*(n - 1)*acc
  }
tailFac(5,1)
  /** 5%
    * 請用Recursive方式找出List中最大數字
    * ex:
    * input:  List(1, 5, 7, 2, 1)
    * output: 7
    * */
  def maxRecur(list: List[Int]): Int = 7
  maxRecur(List(1,5,7,2,1))

  /** 10%
    * 請用Recursive反轉List
    * ex:
    * input:  List(1.1, 2.2, 3.3)
    * output: List(3.3, 2.2, 1.1)
    * */
  def reverseRecur(list: List[Double]): List[Double] = {}
  reverseRecur(List(1.1,2.2,3.3))

  /** 30%
    * 計算List中每個字串出現的次數
    * ex:
    * input:  List("Apple", "Banana", "Apple", "Cherry", "Cherry", "Apple")
    * output: Map(Apple->3, Banana->1, Cherry->2)
    * */
  def wordCount(words: List[String]) = {
    words.groupBy(i => i).mapValues(i => i.length)
  }

  wordCount(List("Apple", "Banana", "Apple", "Cherry", "Cherry", "Apple"))

  /** 30%
    * 計算List中每個數字由左至右的累加值
    * ex:
    * input:  List(1, 2, 3, 4, 5)
    * output: List(1, 3, 6, 10, 15)
    * */
  def accumulator(nums: List[Int]) = {
    nums:
  }
  accumulator(List(1, 2, 3, 4, 5))

  /** 20%
    * 有編號1到1000的球，請寫出一個以取後不放回方式隨機取出n顆球的函數
    * (取出n顆球的順序是隨機的並且不能重複)
    * ex:
    * input:  3
    * output: List(65, 3, 134) 結果為隨機的
    * input:  5
    * output: List(45, 768, 476, 92, 134) 結果為隨機的
    * */
  def selectWithoutReplace(n: Int): List[Int] = {
    val balls = (1 to 1000).toList
    rnd=Random}
  selectWithoutReplace(3)
  selectWithoutReplace(5)

  /** 20%
    * 請將巢狀集合攤平成集合
    * ex:
    * input:  List(List(1),List(2,2),List(3,3,3))
    * output: List(1,2,2,3,3,3)
    * 提示:
    * 你可以考慮使用List本身提供的flatMap或者是:::這兩種方法
    * 當然也可以發揮你的創意，想想看其他作法。
    * */
  def flatten(nestedList: List[List[Int]]): List[Int] = {
    (List + () + Int)
  }

}
